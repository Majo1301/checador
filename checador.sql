create database checador;
use checador;

create table tipopersonal(
id_tipop int auto_increment primary key,
tipo_personal varchar(100));

create table personal(
rfc varchar(20) primary key not null,
nombre varchar(100),
apellido_p varchar(50),
apellido_m varchar(50),
fecha_n varchar(20),
fk_tipopersonal varchar(50));

create table checadas(
id_checada int auto_increment primary key,
usuario varchar(50),
fecha varchar(50),
hora time,
tipo varchar(15));

create table horario_1(
id_horario int primary key auto_increment,
fk_personal varchar(50),
dia_trabajo varchar(20),
hora_entrada time, 
hora_salida time,
foreign key(fk_personal)references personal(rfc));

create table reporte(
id_reporte int primary key auto_increment,
fk_personal varchar(50),
fk_horario int,
fk_checadas int,
observacion varchar(20),
Foreign key(fk_personal) references personal(rfc),
Foreign key(fk_horario) references horario_1(id_horario),
Foreign key(fk_checadas) references checadas(id_checada));

insert into horario_1 values(null,'39589LMJLL','Lunes','08:00:00','2:00:00');
insert into horario_1 values(null,'39589LMJLL','Miercoles','11:00:00','03:00:00');
insert into horario_1 values(null,'44JMCPORUII','Lunes','07:00:00','10:00:00');

select hora_entrada, dia_trabajo, hora,tipo from horario_1,checadas where dia_trabajo='Lunes' and fk_personal='44JMCPORUII';
select hora_entrada, dia_trabajo, hora,tipo from horario_1,checadas where fk_personal='44JMCPORUII' and tipo='Entrada';
